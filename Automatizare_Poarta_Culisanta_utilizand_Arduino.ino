#include <Keypad.h>
#include <EEPROM.h>
#include <rm4.h>                                            // biblioteca RM4 pt radio telecomanda

#define INITIALIZARE 0
#define DESCHISA 1
#define INCHISA 2
#define DESCHIDERE 3
#define INCHIDERE 4

#define DIMENSIUNEPIN 4
#define ADRESAEEPPPIN 0x00                                  //adresa de inceput a zonei de memorie din EEPROM unde este stocat PIN-ul
#define ADRESAEEPFLAG 0x04                                  //adresa la care este stocat flag-ul ce ne indica faptul ca avem deja un PIN stocat in EEPROM

#define TELEC_A 2
#define TELEC_B 8

//Variabile parola acces
char PIN_poarta[DIMENSIUNEPIN];
char pin_EEPROM[DIMENSIUNEPIN] = {'1','2','3','4'};

//Variabila stare poarta
int stare_poarta = INITIALIZARE;                             //INITIALIZARE, DESCHISA, INCHISA, DESCHIDERE, INCHIDERE

//Variabile tastatura numerica 4x4
char tasta = 0;

const byte RAND = 4;  //patru randuri
const byte COL = 4;   //patru coloane

char taste[RAND][COL] = { { '1', '2', '3', 'A' },
                          { '4', '5', '6', 'B' },
                          { '7', '8', '9', 'C' },
                          { '*', '0', '#', 'D' } };

byte pini_randuri[RAND] = { 41, 40, 39, 38 };                  //pinii randurilor tastaturii
byte pini_coloane[COL] = { 37, 36, 35, 34 };                   //pinii coloanelor tastaturii

//initializare instanta clasa "tastatura"
Keypad tastatura = Keypad(makeKeymap(taste), pini_randuri, pini_coloane, RAND, COL);

//Variabile pushbutton si microswitch-uri
const int push_button = 19;
int stare_pushbutton = 0;

const int microsw_inchidere = 2;
volatile int stare_microsw_inchidere = 1;                      //initializat cu 1 deoarece aceasta este starea lui in pozitia "deschis" datorita rezistentei interne pull-up

const int microsw_deschidere = 3;
volatile int stare_microsw_deschidere = 1;                     //initializat cu 1 deoarece aceasta este starea lui in pozitia "deschis" datorita rezistentei interne pull-up

//Variabile Modul L298N cu driver (H-bridge)
const int driver_enable = 46;
const int driver_IN1 = 45;
const int driver_IN2 = 44;

//Radio telecomanda
static const int telecEnable = 13;                              // pin 13 Arduino pin conectat la receiver VCC in. Setat ca HIGH pentru activarea receiver-ului
//Pini de date pt receiver
static const int telecData0 = 9;
static const int telecData1 = 10;
static const int telecData2 = 11;
static const int telecData3 = 12;

// Creare obiect RM4 pt citire coduri butoane de la telecomanda
RM4 telecomanda(telecData0, telecData1, telecData2, telecData3);

void setup()
{
  //Initializare comunicare seriala cu baud rate 9600 (nr de biti transferati)
  Serial.begin(9600);

  //Configurare buton si microswitch-uri ca input-uri
  pinMode(push_button, INPUT_PULLUP);
  pinMode(microsw_inchidere, INPUT_PULLUP);     // Normal deschise - cand nu sunt apasate se citeste 1 digital
  pinMode(microsw_deschidere, INPUT_PULLUP);    // Normal deschise - cand nu sunt apasate se citeste 1 digital

  //Intreruperi microswitch-uri
  attachInterrupt(digitalPinToInterrupt(push_button), intrerupere_pushbutton, RISING);                 //RISING edge - cand semnalul se schimba la LOW la HIGH, pt. ca butonul este normal inchis
  attachInterrupt(digitalPinToInterrupt(microsw_inchidere), intrerupere_microsw_inchidere, FALLING);   //FALLING edge - cand semnalul se schimba de la HIGH pe LOW
  attachInterrupt(digitalPinToInterrupt(microsw_deschidere), intrerupere_microsw_deschidere, FALLING); //FALLING edge - cand semnalul se schimba de la HIGH pe LOW

  //Activare receiver.
  pinMode(telecEnable, OUTPUT);
  digitalWrite(telecEnable, HIGH);
}

void loop()
{
  const int telec_buton = telecomanda.buttonCode();

  //PIN tastatura
  bool status_PIN = false;
  status_PIN = verificarePIN();

  //Mașina de stări
  switch(stare_poarta)
  {
    // Sistemul se alfa in starea de CALIBRARE
    case INITIALIZARE:
      Serial.println("INITIALIZARE");
      //citire si initializare EEPROM, dupa caz
      initializare_EEPROM();
      //calibrare poarta(in momentul in care sistemul este alimentat nu cumoastem pozitia portii, o vom determina acum)
      initializare_POARTA();
      break;

    // Sistemul se alfa in starea de DESCHISA
    case DESCHISA:
      //Push-button-ul a fost apasat sau butonul B al telecomenzii a fost apasat
      if((stare_pushbutton == 1) || (telec_buton == TELEC_B))                     
      {
        Serial.println("DESCHISA -> INCHIDERE");
        stare_microsw_deschidere = 0;
        stare_microsw_inchidere = 0;
        inchidere_poarta();
        stare_pushbutton = 0;
      }
      else
      {
        //nu se executa cod
      }
      break;

      // Sistemul se alfa in starea INCHISA
    case INCHISA:
      //Push-button-ul a fost apasat sau butonul A al telecomenzii a fost apasat
       if((stare_pushbutton == 1) || (telec_buton == TELEC_A) || (false!=status_PIN))
      {
        Serial.println("INCHISA -> DESCHIDERE");
        stare_microsw_deschidere = 0;
        stare_microsw_inchidere = 0;
        deschidere_poarta();
        stare_pushbutton = 0;
      }
      else
      {
        //nu se executa cod
      }
      break;

      // Sistemul se afla in starea de DESCHIDERE
    case DESCHIDERE:
      if(1 == stare_microsw_deschidere)
      {
        oprire_miscare();
        stare_poarta = DESCHISA;
        stare_microsw_deschidere = 0;
        stare_pushbutton = 0;
        Serial.println("DESCHIDERE ->DESCHIS");
      }
      break;

      // Sistemul se alfa in starea INCHIDERE
    case INCHIDERE:
      if(1 == stare_microsw_inchidere)
      {
        oprire_miscare();
        stare_poarta = INCHISA;
        stare_microsw_inchidere = 0;
        stare_pushbutton = 0;
        Serial.println("INCHIDERE -> INCHIS");
      }
      break;
  }
}

/*****************************************************************************/
//Functii atasate intreruperilor pentru buton si microswitch-uri
void intrerupere_pushbutton()
{
  if (stare_pushbutton == 0)
  {
    stare_pushbutton = 1;
    Serial.println("Stare pushbutton: 1");
  }
}

void intrerupere_microsw_inchidere()
{
  if (stare_microsw_inchidere == 0)
  {
    stare_microsw_inchidere = 1;                           //digital read este 0 din cauza ca pin-ul este setat cu INPUT_PULLUP
    Serial.println("Stare microswitch stanga: 1");
  }
}

void intrerupere_microsw_deschidere()
{
  if (stare_microsw_deschidere == 0)
  {
    stare_microsw_deschidere = 1;                           //digital read este 0 din cauza ca pin-ul este setat cu INPUT_PULLUP
    Serial.println("Stare microswitch dreapta: 1");
  }
}

/*CALIBRARE POARTA**************************************************************/
//functie de initializare a memoriei EEPROM
void initializare_EEPROM() {
  char flagPIN = 0;
  int it3;
  flagPIN = EEPROM.read(ADRESAEEPFLAG);
  if (flagPIN != 1)
  {
    for (it3 = 0; it3 < DIMENSIUNEPIN; it3++)
    {
      EEPROM.write((ADRESAEEPPPIN + it3), pin_EEPROM[it3]);
    }
    EEPROM.write(ADRESAEEPFLAG, 1);                          //valoarea de baza 1234 a pin-ului a fost stocata in EEPROM
  }
  else                                                       //este deja o valoare stocata in EEPROM pentru PIN - o citim si o salvam in variabila globala folosita la validarea pin-ului in timpul rularii
  {
    for (it3 = 0; it3 < DIMENSIUNEPIN; it3++) {
      pin_EEPROM[it3] = EEPROM.read((ADRESAEEPPPIN + it3));
    }
  }
}

//functie de initializare si determinare a pozitiei portii
void initializare_POARTA()
{
  stare_microsw_inchidere = digitalRead(microsw_inchidere);
  stare_microsw_deschidere = digitalRead(microsw_deschidere);
  if (false == stare_microsw_inchidere) 
  {
    stare_poarta = INCHISA;
    Serial.println("Poarta initializata ca: INCHISA");
  } 
  else if (false == stare_microsw_deschidere) 
  {
    stare_poarta = DESCHISA;
    Serial.println("Poarta initializata ca: DESCHISA");
  } 
  else 
  {
    stare_microsw_inchidere = 0;
    stare_microsw_deschidere = 0;
    deschidere_poarta();
  }
  
}

/*ACTIONARE POARTA***************************************************************************/
void deschidere_poarta()
{
  digitalWrite(driver_enable, 1);
  digitalWrite(driver_IN1, 1);
  digitalWrite(driver_IN2, 0);
  stare_poarta = DESCHIDERE;
  Serial.println("DESCHIDERE POARTA");
}

void inchidere_poarta()
{
  digitalWrite(driver_enable, 1);
  digitalWrite(driver_IN1, 0);
  digitalWrite(driver_IN2, 1);
  stare_poarta = INCHIDERE;
  Serial.println("INCHIDERE POARTA");
}

void oprire_miscare() 
{
    digitalWrite(driver_enable, 0);
    digitalWrite(driver_IN1, 0);
    digitalWrite(driver_IN2, 0);
}

/*********************************************************************************************/
bool verificarePIN()
{
  bool validitate_PIN = false;
  static int it1 = 0;
  int it2 = 0;
  
  tasta = tastatura.getKey();
  if(tasta)
  {
    PIN_poarta[it1++] = tasta;
    Serial.print(tasta);
  }
  
  if(it1 == DIMENSIUNEPIN)
  {
    delay(200);
    if(!(strncmp(PIN_poarta, pin_EEPROM,DIMENSIUNEPIN)))
    {
      Serial.println("PIN corect");
      validitate_PIN = true;
      it1 = 0; 
    }
    else
    {
      Serial.println("PIN incorect");
      validitate_PIN = false;
      it1 = 0;
    }
  }
  return validitate_PIN;
}
